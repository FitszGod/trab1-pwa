# Trabalho 1 de Programação Web Avançada

# Alunos

1 - Alberto Gusmão Cabral Junior, R.A : 0056119

2 - Gabriel Fernandes Gondim , R.A : 0027967

3 - Daniel Antônio de Sá, R.A : 0023648

<p>

    Dependencias utilizadas para o trabalho

    1 - Bibliotecas Nativas do React

    Comando para as dependencias nativas: npm install

    2 - Axios (para consumo de APIs)
    
    Comando para instalar o axios: npm install axios

    3 - Prime React Prime Icons

    Comando para instalar o Prime Icons: npm install primereact primeicons

    4 - Prime React Route (para rotas de componentes)

    Comando para instalar o Prime React Route: npm install react-router-dom@6


</p>
___________________________________________________________________________________

CONFIGURAÇÕES DO PROJETO

___________________________________________________________________________________
Node Version: 17.9.0

npm: 8.5.5

https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04-pt

______________________________________________________
INSTAL DEPENDECIAS
_______________________________________________________
npm install primereact --save

npm install primeicons --save

npm install classnames --save

npm install react-router-dom --save

----------------------------------------
UPODATE NODE
----------------------------------------

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh

source ~/.bashrc

nvm list-remote

nvm install v17.9.0
